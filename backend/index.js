const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const cors = require('cors');
const app = express();

const fs = require('fs');
const https = require('https');

// Certificate
const privateKey = fs.readFileSync('/etc/letsencrypt/live/my-matrescence.ddns.net/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/my-matrescence.ddns.net/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/my-matrescence.ddns.net/chain.pem', 'utf8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};

// Start https server
const httpsServer = https.createServer(credentials, app);

httpsServer.listen(5500, () => {
	console.log('HTTPS Server running on port 5500');
});

// load middlewares
app.use(bodyParser.json({
    limit: '500kb',
    extended: true
}));

app.use(bodyParser.urlencoded({
    limit: '500kb',
    extended: true
}));

app.use(cors());


const auth = {
    type: 'login',  // other option is ‘oauth2’ 
    user: 'my.matrescence@outlook.com',
    pass: 'Uvi7Mf4VJKnQnxN'
};

// the transport configuration object
const transport = {
    host: 'smtp.office365.com',
    port: 587,
    secure: false,   // true for 465, false for other ports
    tls: { ciphers: 'SSLv3' },
    auth: auth
};

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport(transport);

// verify connection configuration
transporter.verify(function (error, success) {
    if (error) {
        console.log(error);
    } else {
        console.log("My Matrescence Server is ready");
    }
});

app.post('/api/v1/contact', (req, res) => {
    const data = req.body;
    const message = {
        // the address of the senter
        from: {
            name: 'My Matrescence Mailer',
            address: 'my.matrescence@outlook.com',
        },
        // recipients email addresses that will appear on the To: field
        to: data.to,
        cc: data.cc,
        bcc: data.bcc,
        subject: data.subject,
        // the plaintext version of the message as an Unicode string, Buffer, Stream or an attachment-like object ({path: ‘/var/data/…’})
        text: data.text,
        // the HTML version of the message as an Unicode string, Buffer, Stream or an attachment-like object ({path: ‘/var/data/…’})
        html: data.html,
        attachments: [
            {
                contentType: 'image/png',
                content: new Buffer.from(req.body.png, 'base64'),
                encoding: 'binary'
            },
            {
                contentType: 'image/svg+xml',
                content: new Buffer.from(req.body.svg, 'utf-8'),
                encoding: 'binary'
            }
        ],
        // see dsn at https://nodemailer.com/smtp/dsn/
        dsn: {
            id: 'some random message specific id',
            return: 'headers',
            notify: 'success',
            recipient: 'my.matrescence@outlook.com'
        }
    };

    // send the mail
    transporter.sendMail(message, (err /* Error */, info /* SentMessageInfo */) => {
        if (err) {
            res.status(400).send({ err: err });
            // fixme: 
            // response: "432 4.3.2 STOREDRV.ClientSubmit; sender thread limit exceeded [Hostname=AM7PR05MB6947.eurprd05.prod.outlook.com]"
            // responseCode: 432
            // code: "EMESSAGE"
            // command: "DATA"
        } else {
            const { messageId, envelope, accepted, rejected, pending } = info;
            console.log('testMessageUrl = ' + nodemailer.getTestMessageUrl(info));
            console.log('info = ', info);
            console.log(req.requestTime);
            res.status(200).send({ status: 'success', message: message, info: info });
        }
        transporter.close();
    });
});
