import { Component, ViewChild, ElementRef, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { faEnvelope, faDownload, faCircleNotch, faGlobeEurope } from '@fortawesome/free-solid-svg-icons';
import { Router, RoutesRecognized } from '@angular/router';

class Mapper {
  private dt: number;
  private df: number;

  /**
   * @param fromLow the lower bound of the value’s current range.
   * @param fromHigh the upper bound of the value’s current range.
   * @param toLow: the lower bound of the value’s target range.
   * @param toHigh: the upper bound of the value’s target range.
   */
  constructor(private fromLow: number, private fromHigh: number, private toLow: number, private toHigh: number) {
    this.dt = toHigh - toLow;
    this.df = fromHigh - fromLow;
  }

  /**
   * @method map
   * @param value: the number to map.
   */
  map(value: number): number {
    return (value - this.fromLow) * this.dt / this.df + this.toLow;
  }
}

export enum KEY_CODE {
  LEFT_ARROW = 37,
  UP_ARROW = 38,
  RIGHT_ARROW = 39,
  DOWN_ARROW = 40
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild('svg', { static: true }) svgElementRef: ElementRef<SVGSVGElement>;
  readonly faDownload = faDownload;
  readonly faGlobe = faGlobeEurope;
  sendMailInProgress = false;
  lang: 'en' | 'ar' = 'en';

  private mapper: Mapper;

  private shapePath: SVGPathElement;
  private shapePathBBox: DOMRect;
  private shapeCenter: { x: number, y: number };

  private isKnob: boolean;
  private knobPath: SVGPathElement;
  private knobPathBBox: DOMRect;

  private sliderPath: SVGPathElement;
  private sliderPathBBox: DOMRect;

  private origin: { x: number, y: number };

  private minY: number;
  private maxY: number;

  showLangBtn = true;

  formGroup = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)
    ]),
  });

  get faMail() {
    return this.sendMailInProgress ? faCircleNotch : faEnvelope;
  }

  get mailButtonText() {
    return this.sendMailInProgress ? 'Sending ...' : (this.lang === 'en' ? 'Send by E-mail' : 'ارسل بالبريد الإلكترونى');
  }

  get email() {
    return this.formGroup.get('email');
  }

  private knobOffsets = new Map();

  constructor(private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit(): void {

    this.router.events.subscribe((data) => {
      if (data instanceof RoutesRecognized) {
        const params = (data.state as any)._root.value.queryParams;
        if (params['lang']) {
          this.lang = params['lang'];
          console.log(this.lang);
          this.showLangBtn = false;
        }
      }
    });
    
    this.svg.addEventListener('mousedown', this.onStart);
    this.svg.addEventListener('mousemove', this.onMove);
    this.svg.addEventListener('mouseup', this.onEnd);
    // svg.addEventListener('mouseleave', endDrag);
    this.svg.addEventListener('touchstart', this.onStart);
    this.svg.addEventListener('touchmove', this.onMove);
    this.svg.addEventListener('touchend', this.onEnd);
    // svg.addEventListener('touchleave', endDrag);
    this.svg.addEventListener('touchcancel', this.onEnd);

    Array.from(document.getElementsByTagName('path'))
      .filter(path => /sl\d{1,2}_knob/.test(path.id))
      .forEach(knobPath => {
        knobPath.setAttribute('stroke-width', '15');
        knobPath.setAttribute('stroke-opacity', '0');
        knobPath.setAttribute('stroke', '#173E36');
      });
  }

  @HostListener('window:keydown', ['$event'])
  keydownEvent(evt: KeyboardEvent) {
    if (evt.keyCode in KEY_CODE &&
      evt.target instanceof SVGPathElement &&
      /sl\d{1,2}_knob/.test(evt.target.id)) {
        // prevent scroll when moving the knob
      evt.preventDefault();
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyupEvent(evt: KeyboardEvent) {
    if (evt.keyCode in KEY_CODE &&
      evt.target instanceof SVGPathElement &&
      /sl\d{1,2}_knob/.test(evt.target.id)) {
      evt.preventDefault();

      // grab a handler to the corresponding shape
      const id = 'sh' + evt.target.id.match(/\d{1,2}/).join();
      this.shapePath = document.getElementById(id) as any as SVGPathElement;
      this.shapePathBBox = this.shapePath.getBBox();

      // get the shape's rough center
      this.shapeCenter = {
        x: this.shapePathBBox.x + this.shapePathBBox.width / 2,
        y: this.shapePathBBox.y + this.shapePathBBox.height / 2
      };

      // grab a handler to the elements
      this.knobPath = evt.target;
      this.knobPathBBox = this.knobPath.getBBox();

      this.sliderPath = this.knobPath.previousElementSibling as SVGPathElement;
      this.sliderPathBBox = this.sliderPath.getBBox();

      this.minY = this.sliderPathBBox.y - this.knobPathBBox.y;
      this.maxY = this.sliderPathBBox.y + this.sliderPathBBox.height - this.knobPathBBox.y - this.knobPathBBox.height;
      this.mapper = new Mapper(this.minY, this.maxY, 1.5, 0.5);

      const knobPathTransformList: SVGTransformList = this.knobPath.transform.baseVal;

      // Ensure the first transform is a translate transform
      if (knobPathTransformList.numberOfItems === 0 ||
        knobPathTransformList.getItem(0).type !== SVGTransform.SVG_TRANSFORM_TRANSLATE) {
        // Create an transform that translates by (0, 0)
        const translate = this.svg.createSVGTransform();
        translate.setTranslate(0, 0);
        // Add the translation to the front of the transforms list
        this.knobPath.transform.baseVal.insertItemBefore(translate, 0);
      }

      const stepSize = (this.maxY - this.minY) / 10;

      let translateY = this.knobOffsets.get(this.knobPath) || 0;
      if (evt.keyCode === KEY_CODE.LEFT_ARROW || evt.keyCode === KEY_CODE.UP_ARROW) {
        translateY -= stepSize;
      } else {
        translateY += stepSize;
      }

      translateY = Math.min(Math.max(translateY, this.minY), this.maxY);  // confine translateY
      this.knobOffsets.set(this.knobPath, translateY);

      const shapePathTransformList: SVGTransformList = this.shapePath.transform.baseVal;
      // clear previous transformations
      shapePathTransformList.clear();
      const scale = this.mapper.map(translateY);
      // move the origin to (cx,cy)
      const t1 = this.svg.createSVGTransform();
      t1.setTranslate(this.shapeCenter.x, this.shapeCenter.y);
      shapePathTransformList.appendItem(t1);
      // scale
      const t2 = this.svg.createSVGTransform();
      t2.setScale(scale, scale);
      shapePathTransformList.appendItem(t2);
      // move the origin to (0,0)
      const t3 = this.svg.createSVGTransform();
      t3.setTranslate(-this.shapeCenter.x, -this.shapeCenter.y);
      shapePathTransformList.appendItem(t3);

      // clear previous transformations
      knobPathTransformList.clear();
      // move the slider by translateY
      const t4 = this.svg.createSVGTransform();
      t4.setTranslate(0, translateY);
      knobPathTransformList.appendItem(t4);

    }
  }

  get svg() {
    return this.svgElementRef.nativeElement;
  }

  /**
   * @method getMousePosition
   */
  private getMousePosition = (evt: any): { x: number, y: number } => {
    const CTM = this.svg.getScreenCTM();
    if (evt.touches) {
      evt = evt.touches[0];
    }
    return {
      x: (evt.clientX - CTM.e) / CTM.a, // e: translateX, a: scaleX
      y: (evt.clientY - CTM.f) / CTM.d  // f: translateY, d: scaleY
    };
  }

  /**
   * @method onStart
   */
  private onStart = (evt: any) => {

    const isKnob = /sl\d{1,2}_knob/.test(evt.target.id);

    if (isKnob) {
      // we are going to handle the event ourselves
      evt.preventDefault();
      this.isKnob = isKnob;
      // grab a handler to the corresponding shape
      const id = 'sh' + evt.target.id.match(/\d{1,2}/).join([]);
      this.shapePath = document.getElementById(id) as any as SVGPathElement;
      this.shapePathBBox = this.shapePath.getBBox();

      // get the shape's rough center
      this.shapeCenter = {
        x: this.shapePathBBox.x + this.shapePathBBox.width / 2,
        y: this.shapePathBBox.y + this.shapePathBBox.height / 2
      };

      // grab a handler to the elements
      this.knobPath = evt.target;
      this.knobPath.focus();
      this.knobPathBBox = this.knobPath.getBBox();

      this.sliderPath = this.knobPath.previousElementSibling as SVGPathElement;
      this.sliderPathBBox = this.sliderPath.getBBox();

      // get the mouse coordinates relative to the svg
      this.origin = this.getMousePosition(evt);

      // confine dy movement
      this.minY = this.sliderPathBBox.y - this.knobPathBBox.y;
      this.maxY = this.sliderPathBBox.y + this.sliderPathBBox.height - this.knobPathBBox.y - this.knobPathBBox.height;
      this.mapper = new Mapper(this.minY, this.maxY, 1.5, 0.5);

      const knobPathTransformList: SVGTransformList = this.knobPath.transform.baseVal;

      // Ensure the first transform is a translate transform
      if (knobPathTransformList.numberOfItems === 0 ||
        knobPathTransformList.getItem(0).type !== SVGTransform.SVG_TRANSFORM_TRANSLATE) {
        // Create an transform that translates by (0, 0)
        const translate = this.svg.createSVGTransform();
        translate.setTranslate(0, 0);
        // Add the translation to the front of the transforms list
        this.knobPath.transform.baseVal.insertItemBefore(translate, 0);
      }

      // Account previous translations
      const transform = knobPathTransformList.getItem(0);
      this.origin.x -= transform.matrix.e;
      this.origin.y -= transform.matrix.f;
    }
  }

  /**
   * @method onMove
   */
  private onMove = (evt: any): void => {
    if (this.isKnob) {
      evt.preventDefault();
      // grab the mouse position
      const mouse = this.getMousePosition(evt);
      let translateY = mouse.y - this.origin.y;
      // console.log(this.getBoundingBox(this.knobPath), this.knobPathBBox);
      translateY = Math.min(Math.max(translateY, this.minY), this.maxY);  // confine translateY

      this.knobOffsets.set(this.knobPath, translateY);

      const shapePathTransformList: SVGTransformList = this.shapePath.transform.baseVal;
      // clear previous transformations
      shapePathTransformList.clear();
      const scale = this.mapper.map(translateY);
      // move the origin to (cx,cy)
      const t1 = this.svg.createSVGTransform();
      t1.setTranslate(this.shapeCenter.x, this.shapeCenter.y);
      shapePathTransformList.appendItem(t1);
      // scale
      const t2 = this.svg.createSVGTransform();
      t2.setScale(scale, scale);
      shapePathTransformList.appendItem(t2);
      // move the origin to (0,0)
      const t3 = this.svg.createSVGTransform();
      t3.setTranslate(-this.shapeCenter.x, -this.shapeCenter.y);
      shapePathTransformList.appendItem(t3);

      const knobPathTransformList: SVGTransformList = this.knobPath.transform.baseVal;
      // clear previous transformations
      knobPathTransformList.clear();
      // move the slider by translateY
      const t4 = this.svg.createSVGTransform();
      t4.setTranslate(0, translateY);
      knobPathTransformList.appendItem(t4);
    }
  }

  /**
   * @method onEnd
   */
  private onEnd = (evt: any): void => {
    if (this.isKnob) {
      evt.preventDefault();
      this.isKnob = false;
    }
  }

  /**
   * Renders an SVG element to an XML string.
   * @method svgToXML
   */
  private svgToXML = (svg: SVGSVGElement): string => {
    const xml = `<?xml version="1.0" encoding="utf-8"?>\n
      <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">\n
      ${new XMLSerializer().serializeToString(svg)}`;
    // we could also return 'clone.outerHTML' (no IE support)
    return xml;
  }

  /**
   * Renders an SVG element to a Base64 encoded data URI.
   * @param svg the svg element to render
   */
  private svgToDataURL = (svg: SVGSVGElement, callback: (objectURL: string) => void): void => {
    this.svgToBlob(svg, (blob: Blob) => {
      const canvas = document.createElement('canvas');
      canvas.setAttribute('width', svg.getAttribute('width'));
      canvas.setAttribute('height', svg.getAttribute('height'));
      const context = canvas.getContext('2d');

      // give a white background to the canvas
      context.fillStyle = '#fff';
      context.fillRect(0, 0, canvas.width, canvas.height);

      const objectURL = URL.createObjectURL(blob);
      const img = new Image();
      img.addEventListener('load', () => {
        context.drawImage(img, 0, 0);
        URL.revokeObjectURL(objectURL);
        if (callback) {
          callback(canvas.toDataURL('image/png', 1));
        }
        canvas.remove();
      });
      img.addEventListener('error', (err) => {
        console.error('unable to load image resourse ', err);
        URL.revokeObjectURL(objectURL);
        canvas.remove();
      });
      img.src = objectURL;
    });
  }

  /**
   * Renders an SVG element to a Blob.
   * @method svgToBlob
   * @param svg the svg element to render
   */
  private svgToBlob = (svg: SVGSVGElement, callback?: (blob: Blob) => void): Blob => {
    // image/svg+xml;charset=utf-8 - we need to remove ;charset=utf-8 for safari to work
    const blob = new Blob([this.svgToXML(svg)], { type: 'image/svg+xml' });
    if (callback) {
      callback(blob);
    }
    return blob;
  }

  /**
   * Triggers a download of an image rendered from the passed svg document
   * @method saveAsSVG
   */
  saveAsSVG = () => {
    this.svgToBlob(this.svg, (blob: Blob) => {
      const objectURL = URL.createObjectURL(blob);
      saveAs(objectURL, 'my_matrescence.svg');

      // this.triggerDownload(objectURL, 'my_matrescence.svg');
      // safari workaround
      setTimeout(() => {
        URL.revokeObjectURL(objectURL);
      }, 1000);
    });
  }

  saveAsPNG = () => {
    this.svgToDataURL(this.svg, (objectURL) => {
      saveAs(objectURL, 'my_matrescence.png');
      // this.triggerDownload(dataURL, 'my_matrescence.png');
    });
  }

  /**
   * @method sendMail
   */
  sendMail = () => {
    this.svgToDataURL(this.svg, (dataURL) => {
      this.sendMailInProgress = true;
      this.http.post('https://my-matrescence.ddns.net:5500/api/v1/contact', {
        to: this.email.value,
        subject: 'My Matrescence Chart',
        text: this.lang === 'en' ? `
Dear Visitor,

Kindly find your personalized matrescence chart attached. Your moods may change on a daily basis and therefore your chart may look different everyday. Feel free to return to our site and recreate it as many times as you wish. 

Best regards,
Umoumah Exhibition Team
` : `
عزيزي الزائر 

يرجى العثور على مخطط النضج الشخصي المرفق. قد تتغير حالتك المزاجية بشكل يومي وبالتالي قد يبدو مخططك مختلفًا كل يوم. لا تتردد في العودة إلى موقعنا وإعادة إنشائه عدة مرات كما يحلو لك.

تحياتي الحارة،
فريق معرض أمومة
`,
        // html: '<b>&lt;HTML&gt;</b>',
        png: dataURL.split('base64,')[1],
        svg: this.svgToXML(this.svg)
      }).subscribe((response: any) => {
        this.sendMailInProgress = false;
        this.toastr.success(
          `Your matrescence mood chart has been sent to ${this.email.value}`,
          'Great Job!', {
          progressBar: true,
          toastClass: 'toast'
        });
        console.log(response);
      }, (error: any) => {
        this.sendMailInProgress = false;
        this.toastr.error(
          `Something went wrong! We were unable to send your matrescence mood chart. Please try again.`,
          'Oh Snap!', {
          disableTimeOut: true,
          closeButton: true,
          toastClass: 'toast'
        });
        console.error(error);
      });
    });
  }
}
